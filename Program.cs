﻿using System;
using System.Globalization;

namespace NomesObras
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Enunciado: http://dojopuzzles.com/problemas/exibe/nomes-de-autores-de-obras-bibliograficas/*/

            int tam;
            string nomerecebido, sair = "N";
            string[] nomecortado, nomefinal;
            TextInfo myTI = new CultureInfo("pt-br",false).TextInfo;

            while (sair == "N") {
                //Recebendo os valores digitados
                Console.WriteLine("Insira o nome de um autor!");
                nomerecebido = Console.ReadLine();

                //Verificando se o valor recebido não é nulo ou apenas um espaço
                if ( String.IsNullOrWhiteSpace(nomerecebido))  Console.WriteLine("Insira uma palavra!");

                else {
                    //Cortando o nome recebido a cada espaço
                    nomecortado = nomerecebido.Split(" ");
                    tam = nomecortado.Length;

                    //Atribuindo o tamanho do nome já cortado ao nome final!
                    nomefinal = new string[tam];

                    //Quando o nome contém apenas uma palavra
                    if (nomecortado.Length == 1) {
                        Console.WriteLine("Nome do autor estilizado:" + nomecortado[0].ToUpper());
                    }
                    //Quando o nome tem mais que duas palavras e a última palavra atende a uma dessas condições:
                    else if (tam > 2 && (nomecortado[tam-1].ToUpper() == "FILHO" || nomecortado[tam-1].ToUpper() == "FILHA" || nomecortado[tam-1].ToUpper() == "NETO" || nomecortado[tam-1].ToUpper() == "NETA" || nomecortado[tam-1].ToUpper() == "SOBRINHO" || nomecortado[tam-1].ToUpper() == "SOBRINHA" || nomecortado[tam-1].ToUpper() == "JUNIOR")) {
                        
                        //Deixa maiúscula tanto o último nome quanto o penúltimo
                        nomefinal[0] = nomecortado[tam-2].ToUpper();
                        nomefinal[1] = nomecortado[tam-1].ToUpper() + ",";

                        //Como ja preenchemos os dois primeiros lugares do array final (0 e 1), i começa em 2. 
                        //Cont começa em 0 porque ainda não manipulamos os valores nas posições iniciais do array recebido, somente os dois últimos.
                        int i = 2, cont = 0; 
                        while (i < tam) {
                            //Se alguma das palavras recebidos atender a uma dessas condições:
                            if (nomecortado[cont].ToLower() == "da" || nomecortado[cont].ToLower() == "de" || nomecortado[cont].ToLower() == "do" || nomecortado[cont].ToLower() == "das" || nomecortado[cont].ToLower() == "dos") {
                                //Deixa o nome minúsulo
                                nomefinal[i] = nomecortado[cont].ToLower();
                            }
                            else {
                                //Do contrário deixa a primeira letra maiúscula
                                nomefinal[i] = myTI.ToTitleCase(nomecortado[cont]);
                            }
                            i++;
                            cont++;
                        }
                        
                        Console.Write("Nome do autor estilizado: ");
                        //Percorrendo e exibindo o array com o nome estilizado:
                        for (int c =0; c<tam; c++) {
                            Console.Write(nomefinal[c] + " ");
                        }
                    }
                    else {
                        //Definindo o úlimo nome como maiúsculo
                        nomefinal[0] = nomecortado[tam-1].ToUpper() + ",";

                        //Como ja preenchemos o primeiro lugar do array final (0), i começa em 1. 
                        //Cont começa em 0 porque ainda não manipulamos os valores nas posições iniciais do array recebido, somente o último.
                        int i = 1, cont = 0; 
                        while (i < tam) {
                            //Se alguma das palavras recebidos atender a uma dessas condições:
                            if (nomecortado[cont].ToLower() == "da" || nomecortado[cont].ToLower() == "de" || nomecortado[cont].ToLower() == "do" || nomecortado[cont].ToLower() == "das" || nomecortado[cont].ToLower() == "dos") {
                                //Deixa o nome minúsulo
                                nomefinal[i] = nomecortado[cont].ToLower();
                            }
                            else {
                                //Do contrário deixa a primeira letra maiúscula
                                nomefinal[i] = myTI.ToTitleCase(nomecortado[cont]);
                            }
                            i++;
                            cont++;
                        }
                        
                        Console.Write("Nome do autor estilizado:");
                        //Percorrendo e exibindo o array com o nome estilizado:
                        for (int c =0; c<tam; c++) {
                            Console.Write(nomefinal[c] + " ");
                        }
                    }

                    do {
                        Console.WriteLine("\nDeseja sair? (S/N)");
                        sair = Console.ReadLine().ToUpper();

                        if (sair != "S" && sair != "N") {
                            Console.WriteLine("Opção Inválida!");
                        }
                    } while (sair != "S" && sair != "N");
                }
            }

        }
    }
}
